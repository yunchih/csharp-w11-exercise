﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week11_exercise
{
    enum Discount { NULL, A, B}
    class Store
    {
        private int[] Orders;
        private int NumOrders;
        const int MaxOrders = 99;

        public void Open()
        {
            Food[] foods = CreateFoods();
            PrintMenu();
            ReadOrders();

            int discount;
            double total = GetTotal(foods, out discount);
            Echo(foods, Orders, total, discount);
        }

        //**todo**//
        //Create foods
        public static Food[] CreateFoods()
        {
            int numFoods = 7;
            Food[] foods = new Food[numFoods];
            foods[0] = new Food(1, "熱狗堡", 50, Type.主餐);
            foods[1] = new Food(2, "漢堡", 40, Type.主餐);
            foods[2] = new Food(3, "三明治", 30, Type.主餐);
            foods[3] = new Food(4, "薯條", 30, Type.配餐);
            foods[4] = new Food(5, "雞米花", 35, Type.配餐);
            foods[5] = new Food(6, "可樂", 15, Type.飲料);
            foods[6] = new Food(7, "紅茶", 10, Type.飲料);
            return foods;
        }

        //print menu
        public static void PrintMenu()
        {
            Console.WriteLine("編號\t名稱\t價錢\t種類");
            for(int i = 0; i < CreateFoods().Length; ++i)
            {
                Food.Print(CreateFoods(), i);
            }
            Console.Write("\n優惠A：任選主餐+配餐+飲料，打8折。");
            Console.Write("\n優惠B：任選主餐+薯條，折扣20元。");
            Console.Write("\n優惠不得重覆使用，優先度 B > A 。");
        }

        //read order
        public int[] ReadOrders()
        {
            Orders = new int[MaxOrders];
            bool firstTime = true;

            NumOrders = 0;
            for (int i = 0; i < MaxOrders; ++i)
            {
                if(firstTime)
                    Console.WriteLine("\n\n請輸入要點餐的編號，若不需要點餐請輸入0。");
                else
                    Console.WriteLine("請繼續選擇需要的點餐編號，若不需要點餐請輸入0。");

                int order = int.Parse(Console.ReadLine());
                Console.WriteLine();
                if(order == 0)
                    break;

                Orders[i] = order;
                NumOrders++;
                firstTime = false;
            }

            return Orders;
        }
        //gettotal
        public double GetTotal(Food[] foods, out int discount)
        {
            double total = 0;
            if(NumOrders == 0)
            {
                discount = (int)Discount.NULL;
                return total;
            }
            int[] foodTypes = new int[Orders.Length];
            for (int i = 0; i < NumOrders; ++i)
            {
                total += foods[Orders[i] - 1].Price;
                foodTypes[i] = (int)foods[i].Type;
            }

            if (foodTypes.Contains(0) && Orders.Contains(4))
            {
                total = total - 20;
                discount = (int)Discount.B;
                return total;
            }
            else if(foodTypes.Contains(0) && foodTypes.Contains(1) && foodTypes.Contains(2))
            {
                total = total * 0.8;
                discount = (int)Discount.A;
                return total;
            }
            else
            {
                discount = (int)Discount.NULL;
                return total;
            }
        }
        //echo output
        public void Echo(Food[] foods, int[] Orders, double total, int discount)
        {
            string strOrders = "";
            for (int i = 0; i < NumOrders; ++i)
            {
                strOrders += foods[Orders[i] - 1].Name + " ";
            }

            Console.WriteLine("您的餐點為：" + strOrders);

            if (discount != (int)Discount.NULL)
            {
                Console.WriteLine("\n使用{0}", Enum.GetName(typeof(Discount), discount));
            }
            Console.WriteLine("總計為：{0}元", total);
        }
    }
}
