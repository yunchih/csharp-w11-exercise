﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week11_exercise
{
    enum Type {主餐,配餐,飲料};

    class Food
    {
        private int id;
        private string name;
        private double price;
        private Type type;

        public int ID
        {
            get { return id; }
        }
        public string Name
        {
            get { return name; }
        }
        public double Price
        {
            get { return price; }
        }
        public Type Type
        {
            get { return type; }
        }

        public Food(int ID, string Name, double Price, Type Type)
        {
            id = ID;
            name = Name;
            price = Price;
            type = Type;
        }

        public string TypeToString(Type type)
        {
            int numType = Enum.GetValues(typeof(Type)).Length;
            string[] Type = new string[numType];
            return Type[(int)type];
        }

        public static void Print(Food[] foods, int ID)
        {
            Console.WriteLine("{0}\t{1}\t{2}\t{3}", foods[ID].ID, foods[ID].Name, foods[ID].Price, foods[ID].Type);
            return;
        }
    }
}
